import React from "react";

function ExtraDesc({
  isSelected,
  isDisabled,
  extraDesc,
  flavor,
  handleClick
}) {
  const extraDescClass = isDisabled ? 'ExtraDesc--disabled' : ''
  
  return (
    <div className={`ExtraDesc ${extraDescClass}`}>
      {isDisabled 
      ? <p>{`Печалька, ${flavor} закончился`}</p>
      : isSelected 
      ? <p>{ extraDesc }</p>
      : <p>
          {"Чего сидишь, порадуй котэ, "} 
          <span 
            className="ExtraDesc__btn"
            onClick={handleClick}
          >
            купи.
          </span>
        </p>
      }
    </div>
  )
}

export default ExtraDesc