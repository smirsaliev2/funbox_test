import React, {useState} from 'react'
import ExtraDesc from './ExtraDesc'
import Slogan from './Slogan'

function Card({
  name, 
  flavor, 
  contents, 
  imgUrl, 
  weight, 
  slogan, 
  extraDesc,
  isDisabled}) {
  // States
  const [isSelected, setIsSelected] = useState(false)
  const [selectedHover, setSelectedHover] = useState(false)
  // React Elements
  const contentsList = contents.map((i, index) => (<p key={index}>{i}</p>))
  // Styles
  const imgStyle = {
    background: `url('${imgUrl}')`,
    backgroundRepeat: 'no-repeat' 
  }
  // Classes
  const bg = isDisabled ? 'bg-disabled' 
    : selectedHover ? 'bg-selected-hover'
    : isSelected ? 'bg-selected'
    : ''
  const cardMod = isDisabled ? 'card--disabled' : ''
  // Event handlers
  const handleClick = () =>  {
    if (!isDisabled) {
      setIsSelected(prevState => !prevState)
    }
  }
  const handleMouseEnter = () => {
    if (isSelected) {
      setSelectedHover(true)
    }
  }
  const handleMouseLeave = () => {
    if (isSelected) {
      setSelectedHover(false)
    }
  }

  return (
    <div>
      <div className='card-wrapper'>
        <div className={`card-wrapper__border ${bg}`}></div>
        <div className={`card ${cardMod}`}
          onClick={handleClick}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <div className="card__info">
            <Slogan 
              selectedHover={selectedHover} 
              isDisabled={isDisabled}
              slogan={slogan}
            />
            <div className="heading">
              <h1 className='heading__name'>{name}</h1>
              <h3 className='heading__extra'>{flavor}</h3>
            </div>
            <div className='card__contents'>
              {contentsList}
            </div>
          </div>
          <div className="card__media" style={imgStyle}></div>
          <div className={`card__sticker ${bg}`}>
            <p className='card__weight'>{weight}</p>
            <p className='card__kg'>кг</p>
          </div>
        </div>
      </div>

    <ExtraDesc 
      handleClick={handleClick}
      extraDesc={extraDesc} 
      flavor={flavor} 
      isDisabled={isDisabled} 
      isSelected={isSelected}
    />

  </div>
  )
}

export default Card