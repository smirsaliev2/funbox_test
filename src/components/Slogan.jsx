import React from "react";

export default function Slogan({selectedHover, slogan}) {
  const mode = selectedHover ? 'text-selected-hover' : ''
  return (
    <p className={`Slogan ${mode}`}>{ slogan }</p>
  )
}