import './css/App.css'
import CatFood from './pages/CatFood.jsx'

function App() {
  return (
    <div className="App">
      <CatFood />
    </div>
  )
}

export default App
