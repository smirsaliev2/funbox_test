export default [
  {
    id: 0,
    name: 'Нямушка',
    flavor: 'с фуа-гра',
    contents: ['10 порций', 'мышь в подарок'],
    imgUrl: 'images/Photo.png',
    slogan: 'Сказочное заморрское яство',
    extraDesc: 'Печень утки разварная с артишоками',
    weight: 0.5,
    url: '',
    amount: 200,
  }, 
  {
    id: 1,
    name: 'Нямушка',
    flavor: 'с рыбой',
    contents: ['40 порций', '2 мыши в подарок'],
    imgUrl: 'images/Photo.png',
    slogan: 'Сказочное заморское яство',
    extraDesc: 'Головы щучьи с чесноком да свежайшая семгушка',
    weight: 2,
    url: '',
    amount: 100,
  }, 
  {
    id: 2,
    name: 'Нямушка',
    flavor: 'с курой',
    contents: ['100 порций', '5 мышей в подарок', 'заказчик доволен'],
    imgUrl: 'images/Photo.png',
    slogan: 'Сказочное заморское яство',
    extraDesc: 'Филе из цыплят с трюфелями в бульоне',
    weight: 5,
    url: '',
    amount: 0
  }, 
]