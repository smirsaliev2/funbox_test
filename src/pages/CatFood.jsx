import React from 'react'
import data from '../data/catFoodData.js'
import Card from '../components/card.jsx'

function CatFood() {
  const cardList = data.map(i => (<Card 
    key={i.id}
    name={i.name}
    flavor={i.flavor}
    contents={i.contents}
    imgUrl={i.imgUrl}
    weight={i.weight}
    url={i.url}
    slogan={i.slogan}
    extraDesc={i.extraDesc}
    isDisabled={i.amount === 0}
    />))

  return (
    <div className='CatFood container'>
      <h1 className="CatFood__heading">Ты сегодня покормил кота?</h1>
      <div className='card-list'>
        {cardList}
      </div>
    </div>
  )
}

export default CatFood